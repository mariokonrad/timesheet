# UsePackage: use FetchContent to make packages available,
#             either from local or remote repository.

# TODO: documentation

function(use_package)
	include(CMakeParseArguments)
	cmake_parse_arguments(
		ARG
		"" # no values
		"TARGET;GIT_TAG;LOCAL;REMOTE" # single values
		"VARIABLES" # multi values
		${ARGN}
		)

	if(NOT DEFINED ARG_TARGET)
		message(FATAL_ERROR "use_pacakge: TARGET not specified")
	endif()

	if(NOT DEFINED ARG_GIT_TAG)
		message(FATAL_ERROR "use_pacakge: GIT_TAG not specified")
	endif()

	if(NOT DEFINED ARG_LOCAL)
		message(FATAL_ERROR "use_pacakge: LOCAL not specified")
	endif()

	if(NOT DEFINED ARG_REMOTE)
		message(FATAL_ERROR "use_pacakge: REMOTE not specified")
	endif()

	file(TO_CMAKE_PATH "${ARG_LOCAL}" path)
	if(IS_DIRECTORY "${path}")
		set(target_repo "file://${path}")
	else()
		set(target_repo "${ARG_REMOTE}")
	endif()

	if(DEFINED ARG_VARIABLES)
		list(LENGTH ARG_VARIABLES len_total)
		if(${len_total} EQUAL 0)
			message(FATAL_ERROR "use_package: key/value/type tuples must be specified in VARIABLES")
		endif()
		math(EXPR len2 "${len_total} % 3")
		math(EXPR len  "${len_total} / 3 - 1")
		if(NOT len2 EQUAL 0)
			message(FATAL_ERROR "use_package: key/value/type tuples must be specified in VARIABLES")
		endif()
		foreach(index RANGE ${len})
			math(EXPR key_index "${index} * 3")
			math(EXPR val_index "${index} * 3 + 1")
			math(EXPR type_index "${index} * 3 + 2")
			list(GET ARG_VARIABLES ${key_index} key)
			list(GET ARG_VARIABLES ${val_index} val)
			list(GET ARG_VARIABLES ${type_index} type)
			set(${key} ${val} CACHE ${type} \"\" FORCE)
		endforeach()
	endif()

	message(STATUS "use_package: ${ARG_TARGET} from ${target_repo}")

	include(FetchContent)
	FetchContent_Declare(${ARG_TARGET}
		GIT_REPOSITORY "${target_repo}"
		GIT_TAG ${ARG_GIT_TAG}
		)
	FetchContent_MakeAvailable(${ARG_TARGET})
endfunction()

