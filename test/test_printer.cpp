#include "print_brief.hpp"
#include "print_compact_daily_industrial.hpp"
#include "print_daily.hpp"
#include "print_daily_industrial.hpp"
#include "print_balance.hpp"
#include "print_simple.hpp"
#include "print_summary_daily_industrial.hpp"
#include "print_transfer.hpp"
#include "database.hpp"
#include "config.hpp"
#include "range.hpp"
#include <gtest/gtest.h>
#include <sstream>

namespace
{
using timesheet::date;
using timesheet::database;
using timesheet::config;
using timesheet::range;
using timesheet::command;

class test_printer : public ::testing::Test
{
public:
	void SetUp()
	{
		// clang-format off
		db[date::parse("2021-08-01")] = {
			{std::chrono::minutes {120}, "foo"},
			{std::chrono::minutes {60}, "bar"},
			{std::chrono::minutes {60}, "bar"}
			};
		db[date::parse("2021-08-02")] = {
			{std::chrono::minutes {180}, "foo"},
			{std::chrono::minutes {60}, "bar"},
			{std::chrono::minutes {60}, "more time", command::transfer}
			};
		db[date::parse("2021-08-03")] = {
			{std::chrono::minutes {150}, "foo"},
			{-std::chrono::minutes {30}, "less time", command::transfer}
			};
		db[date::parse("2021-08-04")] = {
			{std::chrono::hours{11}, "foo"}
			};
		db[date::parse("2021-08-05")] = {
			{std::chrono::hours{8} + std::chrono::minutes{15}, "baz"}
			};
		db[date::parse("2021-08-06")] = {
			{std::chrono::hours{8} + std::chrono::minutes{20}, "baz"}
			};
		// clang-format on

		cfg.add_minutes_per_day(range{}, std::chrono::hours{8});
	}

protected:
	database db;
	config cfg;

	void test(std::function<void(std::ostream &, const config &, const database &, range)> f,
		const char * range_str, const char * expected)
	{
		std::ostringstream os;
		f(os, cfg, db, range::parse(range_str));
		EXPECT_STREQ(expected, os.str().c_str());
	}
};

TEST_F(test_printer, simple_one_entry)
{
	// clang-format off
	test(timesheet::print_simple, "2021-08-03",
		"2021-08-03 02:30       : foo\n"
		);
	// clang-format on
}

TEST_F(test_printer, simple_two_entries)
{
	// clang-format off
	test(timesheet::print_simple, "2021-08-02",
		"2021-08-02 03:00       : foo\n"
		"2021-08-02 01:00       : bar\n"
		);
	// clang-format on
}

TEST_F(test_printer, simple_three_entries)
{
	// clang-format off
	test(timesheet::print_simple, "2021-08-01",
		"2021-08-01 02:00       : foo\n"
		"2021-08-01 01:00       : bar\n"
		"2021-08-01 01:00       : bar\n"
		);
	// clang-format on
}

TEST_F(test_printer, simple)
{
	// clang-format off
	test(timesheet::print_simple, "..",
		"2021-08-01 02:00       : foo\n"
		"2021-08-01 01:00       : bar\n"
		"2021-08-01 01:00       : bar\n"
		"2021-08-02 03:00       : foo\n"
		"2021-08-02 01:00       : bar\n"
		"2021-08-03 02:30       : foo\n"
		"2021-08-04 11:00       : foo\n"
		"2021-08-05 08:15       : baz\n"
		"2021-08-06 08:20       : baz\n"
		);
	// clang-format on
}

TEST_F(test_printer, brief_one_entry)
{
	// clang-format off
	test(timesheet::print_brief, "2021-08-03",
		"2021-08-03 02:30 (  2.50 -05:30) : 02:30 (foo)\n"
		);
	// clang-format on
}

TEST_F(test_printer, brief_two_entries)
{
	// clang-format off
	test(timesheet::print_brief, "2021-08-02",
		"2021-08-02 04:00 (  4.00 -04:00) : 03:00 (foo) 01:00 (bar)\n"
		);
	// clang-format on
}

TEST_F(test_printer, brief_three_entries)
{
	// clang-format off
	test(timesheet::print_brief, "2021-08-01",
		"2021-08-01 04:00 (  4.00 -04:00) : 02:00 (foo) 01:00 (bar) 01:00 (bar)\n"
		);
	// clang-format on
}

TEST_F(test_printer, brief)
{
	// clang-format off
	test(timesheet::print_brief, "..",
		"2021-08-01 04:00 (  4.00 -04:00) : 02:00 (foo) 01:00 (bar) 01:00 (bar)\n"
		"2021-08-02 04:00 (  4.00 -04:00) : 03:00 (foo) 01:00 (bar)\n"
		"2021-08-03 02:30 (  2.50 -05:30) : 02:30 (foo)\n"
		"2021-08-04 11:00 ( 11.00 +03:00) : 11:00 (foo)\n"
		"2021-08-05 08:15 (  8.25 +00:15) : 08:15 (baz)\n"
		"2021-08-06 08:20 (  8.33 +00:20) : 08:20 (baz)\n"
		);
	// clang-format on
}

TEST_F(test_printer, daily_one_entry)
{
	// clang-format off
	test(timesheet::print_daily, "2021-08-03",
		"2021-08-03 02:30\n"
		);
	// clang-format on
}

TEST_F(test_printer, daily_two_entries)
{
	// clang-format off
	test(timesheet::print_daily, "2021-08-02",
		"2021-08-02 04:00\n"
		);
	// clang-format on
}

TEST_F(test_printer, daily_three_entries)
{
	// clang-format off
	test(timesheet::print_daily, "2021-08-01",
		"2021-08-01 04:00\n"
		);
	// clang-format on
}

TEST_F(test_printer, daily)
{
	// clang-format off
	test(timesheet::print_daily, "..",
		"2021-08-01 04:00\n"
		"2021-08-02 04:00\n"
		"2021-08-03 02:30\n"
		"2021-08-04 11:00\n"
		"2021-08-05 08:15\n"
		"2021-08-06 08:20\n"
		);
	// clang-format on
}

TEST_F(test_printer, daily_industrial_one_entry)
{
	// clang-format off
	test(timesheet::print_daily_industrial, "2021-08-03",
		"2021-08-03   2.50\n"
		);
	// clang-format on
}

TEST_F(test_printer, daily_industrial_two_entries)
{
	// clang-format off
	test(timesheet::print_daily_industrial, "2021-08-02",
		"2021-08-02   4.00\n"
		);
	// clang-format on
}

TEST_F(test_printer, daily_industrial_three_entries)
{
	// clang-format off
	test(timesheet::print_daily_industrial, "2021-08-01",
		"2021-08-01   4.00\n"
		);
	// clang-format on
}

TEST_F(test_printer, daily_industrial)
{
	// clang-format off
	test(timesheet::print_daily_industrial, "..",
		"2021-08-01   4.00\n"
		"2021-08-02   4.00\n"
		"2021-08-03   2.50\n"
		"2021-08-04  11.00\n"
		"2021-08-05   8.25\n"
		"2021-08-06   8.33\n"
		);
	// clang-format on
}

TEST_F(test_printer, compact_daily_industrial_one_entry)
{
	// clang-format off
	test(timesheet::print_compact_daily_industrial, "2021-08-03",
		"2021-08-03   2.50 foo\n"
		);
	// clang-format on
}

TEST_F(test_printer, compact_daily_industrial_two_entries)
{
	// clang-format off
	test(timesheet::print_compact_daily_industrial, "2021-08-02",
		"2021-08-02   1.00 bar\n"
		"2021-08-02   3.00 foo\n"
		);
	// clang-format on
}

TEST_F(test_printer, compact_daily_industrial_three_entries)
{
	// clang-format off
	test(timesheet::print_compact_daily_industrial, "2021-08-01",
		"2021-08-01   2.00 bar\n"
		"2021-08-01   2.00 foo\n"
		);
	// clang-format on
}

TEST_F(test_printer, compact_daily_industrial)
{
	// clang-format off
	test(timesheet::print_compact_daily_industrial, "..",
		"2021-08-01   2.00 bar\n"
		"2021-08-01   2.00 foo\n"
		"2021-08-02   1.00 bar\n"
		"2021-08-02   3.00 foo\n"
		"2021-08-03   2.50 foo\n"
		"2021-08-04  11.00 foo\n"
		"2021-08-05   8.25 baz\n"
		"2021-08-06   8.33 baz\n"
		);
	// clang-format on
}

TEST_F(test_printer, summary_daily_industrial)
{
	// clang-format off
	test(timesheet::print_summary_daily_industrial, "..",
		"2021-08-01 :   4.00\n"
		"               2.00 bar\n"
		"               2.00 foo\n"
		"\n"
		"2021-08-02 :   4.00\n"
		"               1.00 bar\n"
		"               3.00 foo\n"
		"\n"
		"2021-08-03 :   2.50\n"
		"               2.50 foo\n"
		"\n"
		"2021-08-04 :  11.00\n"
		"              11.00 foo\n"
		"\n"
		"2021-08-05 :   8.25\n"
		"               8.25 baz\n"
		"\n"
		"2021-08-06 :   8.33\n"
		"               8.33 baz\n"
		"Summary:\n"
		"  days   : 6\n"
		"  balance: -09:25 (-565 minutes, -9.42 hours)\n"
		"  raw    : -09:55 (-595 minutes, -9.92 hours) (transfer: 0.50 hours)\n"
		"  average:  06:20 (380 minutes, 6.33 hours)\n"
		);
	// clang-format on
}

TEST_F(test_printer, balance_one_entry)
{
	// clang-format off
	test(timesheet::print_balance, "2021-08-03",
		"1 days\n"
		"  balance: -06:00 (-360 minutes, -6.00 hours)\n"
		"  raw    : -05:30 (-330 minutes, -5.50 hours) (transfer: -0.50 hours)\n"
		"  average:  02:30 (150 minutes, 2.50 hours)\n"
		);
	// clang-format on
}

TEST_F(test_printer, balance_two_entries)
{
	// clang-format off
	test(timesheet::print_balance, "2021-08-02",
		"1 days\n"
		"  balance: -03:00 (-180 minutes, -3.00 hours)\n"
		"  raw    : -04:00 (-240 minutes, -4.00 hours) (transfer: 1.00 hours)\n"
		"  average:  04:00 (240 minutes, 4.00 hours)\n"
		);
	// clang-format on
}

TEST_F(test_printer, balance_three_entries)
{
	// clang-format off
	test(timesheet::print_balance, "2021-08-01",
		"1 days\n"
		"  balance: -04:00 (-240 minutes, -4.00 hours)\n"
		"  raw    : -04:00 (-240 minutes, -4.00 hours) (transfer: 0.00 hours)\n"
		"  average:  04:00 (240 minutes, 4.00 hours)\n"
		);
	// clang-format on
}

TEST_F(test_printer, balance)
{
	// clang-format off
	test(timesheet::print_balance, "..",
		"6 days\n"
		"  balance: -09:25 (-565 minutes, -9.42 hours)\n"
		"  raw    : -09:55 (-595 minutes, -9.92 hours) (transfer: 0.50 hours)\n"
		"  average:  06:20 (380 minutes, 6.33 hours)\n"
		);
	// clang-format on
}

TEST_F(test_printer, transfer)
{
	// clang-format off
	test(timesheet::print_transfer, "..",
		"2021-08-02  +01:00  (  1.00)\n"
		"2021-08-03  -00:30  (- 0.50)\n"
		"\n"
		"TOTAL:  +00:30  (  0.50)\n"
		);
	// clang-format on
}
}

