#include "date.hpp"
#include <gtest/gtest.h>

namespace
{
using timesheet::date;

class test_date : public ::testing::Test
{
};

TEST_F(test_date, empty_string)
{
	EXPECT_ANY_THROW(auto r = date::parse(""));
}

TEST_F(test_date, parse_success_stories)
{
	EXPECT_STREQ("2016-01-01", date::parse("2016-01-01").str().c_str());
	EXPECT_STREQ("2016-02-29", date::parse("2016-02-29").str().c_str());
	EXPECT_STREQ("2016-12-31", date::parse("2016-12-31").str().c_str());
	EXPECT_STREQ("2017-01-01", date::parse("2017-01-01").str().c_str());
	EXPECT_STREQ("2017-02-28", date::parse("2017-02-28").str().c_str());
	EXPECT_STREQ("2017-12-31", date::parse("2017-12-31").str().c_str());
	EXPECT_STREQ("2018-01-01", date::parse("2018-01-01").str().c_str());
	EXPECT_STREQ("2018-02-28", date::parse("2018-02-28").str().c_str());
	EXPECT_STREQ("2018-12-31", date::parse("2018-12-31").str().c_str());
}

TEST_F(test_date, parse_failures)
{
	EXPECT_ANY_THROW(date::parse("2016-01-00"));
	EXPECT_ANY_THROW(date::parse("2016-02-30"));
	EXPECT_ANY_THROW(date::parse("2016-12-32"));
	EXPECT_ANY_THROW(date::parse("2016-13-01"));
	EXPECT_ANY_THROW(date::parse("2017-00-01"));
	EXPECT_ANY_THROW(date::parse("2017-02-29"));
	EXPECT_ANY_THROW(date::parse("2018-02-29"));
	EXPECT_ANY_THROW(date::parse("2018-12-0" ));
	EXPECT_ANY_THROW(date::parse("0-01-01"   ));
	EXPECT_ANY_THROW(date::parse("00-01-01"  ));
	EXPECT_ANY_THROW(date::parse("000-01-01" ));
	EXPECT_ANY_THROW(date::parse("0000-00-01"));
	EXPECT_ANY_THROW(date::parse("0000-01-00"));
}

TEST_F(test_date, today)
{
	// unfortunately this test cannot check the specific values,
	// because unit tests cannot change system time and a clock
	// insertion is not desired.

	const auto t = date::today();

	EXPECT_GT(t.year(), 2000);

	EXPECT_GT(t.month(), 0);
	EXPECT_LT(t.month(), 13);

	EXPECT_GT(t.day(), 0);
	EXPECT_LE(t.day(), date::max_mday(t.year(), t.month()));
}

TEST_F(test_date, today_with_offset)
{
	// unfortunately this test cannot check the specific values,
	// because unit tests cannot change system time and a clock
	// insertion is not desired.

	const auto t = date::today(-1);

	EXPECT_GT(t.year(), 2000);

	EXPECT_GT(t.month(), 0);
	EXPECT_LT(t.month(), 13);

	EXPECT_GT(t.day(), 0);
	EXPECT_LE(t.day(), date::max_mday(t.year(), t.month()));
}

TEST_F(test_date, difference)
{
	EXPECT_EQ(::date::days{2}, difference(timesheet::date(2021, 1, 1), timesheet::date(2021, 1, 3)));
	EXPECT_EQ(::date::days{2}, difference(timesheet::date(2021, 1, 3), timesheet::date(2021, 1, 1)));

	EXPECT_EQ(::date::days{0}, difference(timesheet::date(2021, 1, 1), timesheet::date(2021, 1, 1)));
}
}
