#include "database.hpp"
#include <gtest/gtest.h>
#include <sstream>

namespace
{
using timesheet::database;

class test_database : public ::testing::Test
{
};

TEST_F(test_database, time)
{
	const std::string data = "2021-01-01 08:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(1u, db.size());

	const auto entry = db.begin()->second;

	ASSERT_EQ(1u, entry.size());

	const auto x = entry.front();

	EXPECT_EQ(std::chrono::minutes(60) * 8, x.duration());
	EXPECT_STREQ("foobar", x.text.c_str());
}

TEST_F(test_database, single_range)
{
	const std::string data = "2021-01-01 08:00-12:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(1u, db.size());

	const auto entry = db.begin()->second;

	ASSERT_EQ(1u, entry.size());

	const auto x = entry.front();

	EXPECT_EQ(std::chrono::minutes(60) * 4, x.duration());
	EXPECT_STREQ("foobar", x.text.c_str());
}

TEST_F(test_database, multiple_ranges)
{
	const std::string data = "2021-01-01 08:00-12:00 13:00-14:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(1u, db.size());

	const auto entry = db.begin()->second;

	ASSERT_EQ(2u, entry.size());

	const auto x0 = *entry.begin();

	EXPECT_EQ(std::chrono::minutes(60) * 4, x0.duration());
	EXPECT_STREQ("foobar", x0.text.c_str());

	const auto x1 = *std::next(entry.begin());

	EXPECT_EQ(std::chrono::minutes(60) * 1, x1.duration());
	EXPECT_STREQ("foobar", x1.text.c_str());
}

TEST_F(test_database, multiple_ranges_with_semicolon_delimiter)
{
	const std::string data = "2021-01-01 08:00-12:00; 13:00-14:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(1u, db.size());

	const auto entry = db.begin()->second;

	ASSERT_EQ(2u, entry.size());

	const auto x0 = *entry.begin();

	EXPECT_EQ(std::chrono::minutes(60) * 4, x0.duration());
	EXPECT_STREQ("foobar", x0.text.c_str());

	const auto x1 = *std::next(entry.begin());

	EXPECT_EQ(std::chrono::minutes(60) * 1, x1.duration());
	EXPECT_STREQ("foobar", x1.text.c_str());
}

TEST_F(test_database, command_transfer_negative_time)
{
	const std::string data = "2021-01-01 transfer -10:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(1u, db.size());

	const auto entry = db.begin()->second;

	ASSERT_EQ(1u, entry.size());

	const auto x = *entry.begin();

	EXPECT_EQ(timesheet::command::transfer, x.cmd);
	EXPECT_EQ(-std::chrono::hours(10), x.duration());
	EXPECT_STREQ("foobar", x.text.c_str());
}

TEST_F(test_database, command_transfer_positive_time)
{
	const std::string data = "2021-01-01 transfer +10:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(1u, db.size());

	const auto entry = db.begin()->second;

	ASSERT_EQ(1u, entry.size());

	const auto x = *entry.begin();

	EXPECT_EQ(timesheet::command::transfer, x.cmd);
	EXPECT_EQ(std::chrono::hours(10), x.duration());
	EXPECT_STREQ("foobar", x.text.c_str());
}

TEST_F(test_database, command_invalid_case_sensitivity)
{
	const std::string data = "2021-01-01 TRANSFER +10:00 foobar";

	std::istringstream is{data};
	database db;

	EXPECT_ANY_THROW(read_data_txt(db, is));
}

TEST_F(test_database, command_invalid_unknown)
{
	const std::string data = "2021-01-01 none +10:00 foobar";

	std::istringstream is{data};
	database db;

	EXPECT_ANY_THROW(read_data_txt(db, is));
}

TEST_F(test_database, command_transfer_signless_time_causing_exception)
{
	const std::string data = "2021-01-01 transfer 10:00 foobar";

	std::istringstream is{data};
	database db;

	EXPECT_ANY_THROW(read_data_txt(db, is));
}

TEST_F(test_database, command_transfer_range_time_causing_exception)
{
	const std::string data = "2021-01-01 transfer 09:00-10:00 foobar";

	std::istringstream is{data};
	database db;

	EXPECT_ANY_THROW(read_data_txt(db, is));
}

TEST_F(test_database, leading_spaces)
{
	const std::string data = "  2021-01-01 08:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(1u, db.size());

	const auto entry = db.begin()->second;

	ASSERT_EQ(1u, entry.size());

	const auto x = *entry.begin();

	EXPECT_EQ(timesheet::command::none, x.cmd);
	EXPECT_EQ(std::chrono::hours(8), x.duration());
	EXPECT_STREQ("foobar", x.text.c_str());
}

TEST_F(test_database, comment)
{
	const std::string data = "# 2021-01-01 08:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(0u, db.size());
}

TEST_F(test_database, comment_leading_spaces)
{
	const std::string data = "  # 2021-01-01 08:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(0u, db.size());
}

TEST_F(test_database, comment_leading_tabs)
{
	const std::string data = "\t\t# 2021-01-01 08:00 foobar";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(0u, db.size());
}

TEST_F(test_database, comment_arbitrary_text)
{
	const std::string data = "# some text";

	std::istringstream is{data};
	database db;

	read_data_txt(db, is);

	ASSERT_EQ(0u, db.size());
}
}

