#include "time.hpp"
#include <gtest/gtest.h>

namespace
{
class test_time : public ::testing::Test
{
};

TEST_F(test_time, to_minutes)
{
	using timesheet::to_minutes;
	using std::chrono::minutes;

	EXPECT_EQ(+minutes {60}, to_minutes("01:00"));
	EXPECT_EQ(+minutes {60}, to_minutes("+01:00"));
	EXPECT_EQ(-minutes {60}, to_minutes("-01:00"));
	EXPECT_EQ(+minutes {70}, to_minutes("01:10"));
	EXPECT_EQ(+minutes {70}, to_minutes("+01:10"));
	EXPECT_EQ(-minutes {70}, to_minutes("-01:10"));
}

TEST_F(test_time, render_duration_signed)
{
	using timesheet::render_duration_signed;
	using std::chrono::minutes;
	using std::chrono::hours;

	EXPECT_STREQ("+01:00", render_duration_signed(+minutes {60}).c_str());
	EXPECT_STREQ("+01:30", render_duration_signed(+minutes {90}).c_str());
	EXPECT_STREQ("+02:00", render_duration_signed(+minutes {120}).c_str());
	EXPECT_STREQ("+03:00", render_duration_signed(+hours {3}).c_str());
	EXPECT_STREQ("+10:00", render_duration_signed(+hours {10}).c_str());
	EXPECT_STREQ("-01:00", render_duration_signed(-hours {1}).c_str());
	EXPECT_STREQ("-01:30", render_duration_signed(-minutes {90}).c_str());
	EXPECT_STREQ("-02:00", render_duration_signed(-hours {2}).c_str());
	EXPECT_STREQ("-03:00", render_duration_signed(-hours {3}).c_str());
	EXPECT_STREQ("-10:00", render_duration_signed(-hours {10}).c_str());
}

TEST_F(test_time, render_duration_industrial)
{
	using timesheet::render_duration_industrial;
	using std::chrono::minutes;
	using std::chrono::hours;

	EXPECT_STREQ("  1.00", render_duration_industrial(+minutes {60}).c_str());
	EXPECT_STREQ("  1.50", render_duration_industrial(+minutes {90}).c_str());
	EXPECT_STREQ("  2.00", render_duration_industrial(+minutes {120}).c_str());
	EXPECT_STREQ("  3.00", render_duration_industrial(+hours {3}).c_str());
	EXPECT_STREQ("  3.25", render_duration_industrial(+hours {3} + minutes{15}).c_str());
	EXPECT_STREQ("  3.33", render_duration_industrial(+hours {3} + minutes{20}).c_str());
	EXPECT_STREQ("  3.67", render_duration_industrial(+hours {3} + minutes{40}).c_str());
	EXPECT_STREQ("  3.75", render_duration_industrial(+hours {3} + minutes{45}).c_str());
	EXPECT_STREQ(" 10.00", render_duration_industrial(+hours {10}).c_str());
	EXPECT_STREQ("- 1.00", render_duration_industrial(-hours {1}).c_str());
	EXPECT_STREQ("- 1.50", render_duration_industrial(-minutes {90}).c_str());
	EXPECT_STREQ("- 2.00", render_duration_industrial(-hours {2}).c_str());
	EXPECT_STREQ("- 3.00", render_duration_industrial(-hours {3}).c_str());
	EXPECT_STREQ("-10.00", render_duration_industrial(-hours {10}).c_str());
}
}
