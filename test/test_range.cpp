#include "range.hpp"
#include "date.hpp"
#include <gtest/gtest.h>

namespace
{
using timesheet::range;
using timesheet::date;

class test_range : public ::testing::Test
{
};

TEST_F(test_range, empty_string)
{
	EXPECT_ANY_THROW(auto r = range::parse(""));
}

TEST_F(test_range, default_range)
{
	const char * input = "..";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f < t);

	EXPECT_TRUE(f == date::parse("0000-01-01"));
	EXPECT_TRUE(t == date::parse("9999-12-31"));
}

TEST_F(test_range, range_year_until)
{
	const char * input = "2018..";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("2018-01-01"));
	EXPECT_TRUE(t == date::parse("9999-12-31"));
}

TEST_F(test_range, range_year_month_until)
{
	const char * input = "2018-02..";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("2018-02-01"));
	EXPECT_TRUE(t == date::parse("9999-12-31"));
}

TEST_F(test_range, range_year_month_day_until)
{
	const char * input = "2018-03-12..";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("2018-03-12"));
	EXPECT_TRUE(t == date::parse("9999-12-31"));
}

TEST_F(test_range, range_until_year)
{
	const char * input = "..2018";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("0000-01-01"));
	EXPECT_TRUE(t == date::parse("2018-12-31"));
}

TEST_F(test_range, range_until_year_month)
{
	const char * input = "..2018-03";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("0000-01-01"));
	EXPECT_TRUE(t == date::parse("2018-03-31"));
}

TEST_F(test_range, range_until_year_month_feb_nonleap)
{
	const char * input = "..2018-02";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("0000-01-01"));
	EXPECT_TRUE(t == date::parse("2018-02-28"));
}

TEST_F(test_range, range_until_year_month_feb_leap)
{
	const char * input = "..2016-02";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("0000-01-01"));
	EXPECT_TRUE(t == date::parse("2016-02-29"));
}

TEST_F(test_range, range_until_year_month_day)
{
	const char * input = "..2018-02-23";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("0000-01-01"));
	EXPECT_TRUE(t == date::parse("2018-02-23"));
}

TEST_F(test_range, range_year_year)
{
	const char * input = "2018..2019";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("2018-01-01"));
	EXPECT_TRUE(t == date::parse("2019-12-31"));
}

TEST_F(test_range, range_year_month_year)
{
	const char * input = "2018-02..2019";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("2018-02-01"));
	EXPECT_TRUE(t == date::parse("2019-12-31"));
}

TEST_F(test_range, range_year_month_day_year)
{
	const char * input = "2018-02-23..2019";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == date::parse("2018-02-23"));
	EXPECT_TRUE(t == date::parse("2019-12-31"));
}

TEST_F(test_range, range_contains_date)
{
	struct testcase { const char * d; bool expected; };

	static const testcase cases[] = {
		{ "2016-12-31", false },
		{ "2017-01-01", true },
		{ "2017-01-02", true },
		{ "2017-02-01", true },
		{ "2018-03-14", true },
		{ "2018-03-15", true },
		{ "2018-03-16", false },
	};

	const auto r = range::parse("2017-01-01..2018-03-15");

	for (const auto & tc : cases)
		EXPECT_TRUE(r.contains(date::parse(tc.d)) == tc.expected);
}

TEST_F(test_range, single_date_year_month_day)
{
	const char * input = "2018-03-09";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_TRUE(f == t);

	EXPECT_TRUE(f == date::parse("2018-03-09"));
	EXPECT_TRUE(t == date::parse("2018-03-09"));
}

TEST_F(test_range, single_date_year_month)
{
	const char * input = "2018-03";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_FALSE(f == t);

	EXPECT_TRUE(f == date::parse("2018-03-01"));
	EXPECT_TRUE(t == date::parse("2018-03-31"));
}

TEST_F(test_range, single_date_year_month_feb_leap)
{
	const char * input = "2016-02";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_FALSE(f == t);

	EXPECT_TRUE(f == date::parse("2016-02-01"));
	EXPECT_TRUE(t == date::parse("2016-02-29"));
}

TEST_F(test_range, single_date_year_month_feb_nonleap)
{
	const char * input = "2018-02";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_FALSE(f == t);

	EXPECT_TRUE(f == date::parse("2018-02-01"));
	EXPECT_TRUE(t == date::parse("2018-02-28"));
}

TEST_F(test_range, single_date_year)
{
	const char * input = "2018";

	const auto r = range::parse(input);
	const auto f = r.from();
	const auto t = r.to();

	EXPECT_FALSE(f == t);

	EXPECT_TRUE(f == date::parse("2018-01-01"));
	EXPECT_TRUE(t == date::parse("2018-12-31"));
}

TEST_F(test_range, single_date_failure)
{
	EXPECT_ANY_THROW(range::parse("2018-03-1"));
	EXPECT_ANY_THROW(range::parse("2018-12-1"));
	EXPECT_ANY_THROW(range::parse("2018-12-32"));
}

TEST_F(test_range, today_check_parser)
{
	EXPECT_NO_THROW(range::parse("today"));
}

TEST_F(test_range, today_with_offset_check_parser)
{
	EXPECT_NO_THROW(range::parse("today-1"));
	EXPECT_NO_THROW(range::parse("today-10"));
	EXPECT_NO_THROW(range::parse("today-100"));

	EXPECT_ANY_THROW(range::parse("today-"));
	EXPECT_ANY_THROW(range::parse("today-0"));
}

TEST_F(test_range, today_check_dates)
{
	const auto today = date::today();

	const auto r = range::parse("today");

	EXPECT_TRUE(r.from() == today);
	EXPECT_TRUE(r.to() == today);
}

TEST_F(test_range, today_with_offset_check_dates)
{
	const auto t = date::today(-2);

	const auto r = range::parse("today-2");

	EXPECT_TRUE(r.from() == t);
	EXPECT_TRUE(r.to() == t);
}

TEST_F(test_range, week_check_dates)
{
	const auto w = date::this_week();

	const auto r = range::parse("week");

	EXPECT_EQ(w.first, r.from());
	EXPECT_EQ(w.second, r.to());
}

TEST_F(test_range, week_with_offset_check_dates)
{
	const auto w = date::this_week(-2);

	const auto r = range::parse("week-2");

	EXPECT_EQ(w.first, r.from());
	EXPECT_EQ(w.second, r.to());
}

TEST_F(test_range, month_check_dates)
{
	const auto m = date::this_month();

	const auto r = range::parse("month");

	EXPECT_EQ(m.first, r.from());
	EXPECT_EQ(m.second, r.to());
}

TEST_F(test_range, month_with_offset_check_dates)
{
	const auto m = date::this_month(-2);

	const auto r = range::parse("month-2");

	EXPECT_EQ(m.first, r.from());
	EXPECT_EQ(m.second, r.to());
}

TEST_F(test_range, year_check_dates)
{
	const auto m = date::this_year();

	const auto r = range::parse("year");

	EXPECT_EQ(m.first, r.from());
	EXPECT_EQ(m.second, r.to());
}

TEST_F(test_range, year_with_offset_check_dates)
{
	const auto m = date::this_year(-3);

	const auto r = range::parse("year-3");

	EXPECT_EQ(m.first, r.from());
	EXPECT_EQ(m.second, r.to());
}
}
