#include "config.hpp"
#include "date.hpp"
#include "range.hpp"
#include <gtest/gtest.h>

namespace
{
using timesheet::config;
using timesheet::range;

class test_config : public ::testing::Test
{
};

TEST_F(test_config, default_values)
{
	config cfg;

	EXPECT_EQ(std::chrono::minutes {480}, cfg.minutes_per_day(timesheet::date(2000, 1, 1)));
}

TEST_F(test_config, add_minutes_per_day)
{
	config cfg;

	static const auto d = timesheet::date(2000, 1, 1);

	ASSERT_NE(std::chrono::minutes {10}, cfg.minutes_per_day(d));
	cfg.add_minutes_per_day(range{}, std::chrono::minutes {10});
	EXPECT_EQ(std::chrono::minutes {10}, cfg.minutes_per_day(d));
}

TEST_F(test_config, add_minutes_per_day_with_invalid_values)
{
	config cfg;

	EXPECT_ANY_THROW(cfg.add_minutes_per_day(range{}, std::chrono::minutes {0}));
	EXPECT_ANY_THROW(cfg.add_minutes_per_day(range{}, std::chrono::hours {24}));
	EXPECT_ANY_THROW(
		cfg.add_minutes_per_day(range {}, std::chrono::hours {24} + std::chrono::minutes {1}));
}

TEST_F(test_config, override_input_filepath)
{
	config cfg;

	EXPECT_TRUE(cfg.input_filepaths().empty());
	cfg.insert_input_filepath("foobar.txt");
	EXPECT_FALSE(cfg.input_filepaths().empty());
	const auto paths = cfg.input_filepaths();
	EXPECT_EQ(1u, paths.size());
	EXPECT_STREQ("foobar.txt", paths[0].c_str());
}

TEST_F(test_config, override_input_filepath_with_invalid_path)
{
	config cfg;

	EXPECT_ANY_THROW(cfg.insert_input_filepath(""));
}

TEST_F(test_config, read_from_string)
{
	static const std::string s = "{ \"minutes_per_workday\" : { \"..\" : 20 } }";
	static const auto d = timesheet::date(2000, 1, 1);

	ASSERT_NO_THROW(timesheet::read_config_string(s));

	config cfg = timesheet::read_config_string(s);
	EXPECT_EQ(std::chrono::minutes {20}, cfg.minutes_per_day(d));
}

TEST_F(test_config, read_from_string_multiple_range_for_minutes_per_workday)
{
	static const std::string s = "{ \"minutes_per_workday\" : { \"..\" : 20, \"2000..2001\": 30 } }";

	config cfg = timesheet::read_config_string(s);
	EXPECT_EQ(std::chrono::minutes {20}, cfg.minutes_per_day(timesheet::date(1999, 1, 1)));
	EXPECT_EQ(std::chrono::minutes {20}, cfg.minutes_per_day(timesheet::date(1999, 12, 31)));
	EXPECT_EQ(std::chrono::minutes {30}, cfg.minutes_per_day(timesheet::date(2000, 1, 1)));
	EXPECT_EQ(std::chrono::minutes {30}, cfg.minutes_per_day(timesheet::date(2000, 12, 31)));
	EXPECT_EQ(std::chrono::minutes {30}, cfg.minutes_per_day(timesheet::date(2001, 1, 1)));
	EXPECT_EQ(std::chrono::minutes {30}, cfg.minutes_per_day(timesheet::date(2001, 12, 31)));
	EXPECT_EQ(std::chrono::minutes {20}, cfg.minutes_per_day(timesheet::date(2002, 1, 1)));
}

TEST_F(test_config, read_from_string_invalid_structure_minutes_per_workday)
{
	static const std::string s = "{ \"minutes_per_workday\" : 20 }";

	EXPECT_ANY_THROW(timesheet::read_config_string(s));
}

TEST_F(test_config, read_from_empty_string)
{
	EXPECT_ANY_THROW(timesheet::read_config_string(""));
}

TEST_F(test_config, read_from_nonexistent_file)
{
	EXPECT_ANY_THROW(timesheet::read_config_file("/this/file/does/not/exist"));
	EXPECT_ANY_THROW(timesheet::read_config_file(""));
}

TEST_F(test_config, single_filepath)
{
	static const std::string s = "{ \"input_filepath\": \"somefile.txt\" }";

	ASSERT_NO_THROW(timesheet::read_config_string(s));

	config cfg = timesheet::read_config_string(s);
	EXPECT_EQ(1u, cfg.input_filepaths().size());
}

TEST_F(test_config, single_filepath_in_array)
{
	static const std::string s = "{ \"input_filepath\": [ \"somefile.txt\" ] }";

	ASSERT_NO_THROW(timesheet::read_config_string(s));

	config cfg = timesheet::read_config_string(s);
	EXPECT_EQ(1u, cfg.input_filepaths().size());
}

TEST_F(test_config, multiple_filepaths)
{
	static const std::string s
		= "{ \"input_filepath\": [ \"somefile.txt\", \"anotherfile.txt\" ] }";

	ASSERT_NO_THROW(timesheet::read_config_string(s));

	config cfg = timesheet::read_config_string(s);
	EXPECT_EQ(2u, cfg.input_filepaths().size());
}

TEST_F(test_config, replace_environment_variables)
{
	{
		std::string text = "bar";
		auto result = timesheet::replace_environment_variables(
			"{FOO}", [&](const char *) -> char * { return text.data(); });
		EXPECT_STREQ("bar", result.c_str());
	}
	{
		std::string text = "";
		auto result = timesheet::replace_environment_variables(
			"{FOO}/{BAR}/test.txt", [&](const char * v) -> char * {
				const auto var = std::string(v);
				if (var == "FOO")
					text = "foo";
				if (var == "BAR")
					text = "bar";
				return text.data();
			});
		EXPECT_STREQ("foo/bar/test.txt", result.c_str());
	}
}

TEST_F(test_config, replace_environment_variables_multipass)
{
	std::string text = "";
	auto result = timesheet::replace_environment_variables(
		"{F{xx}}/{BAR}/test.txt", [&](const char * v) -> char * {
			const auto var = std::string(v);
			if (var == "xx")
				text = "OO";
			if (var == "FOO")
				text = "foo";
			if (var == "BAR")
				text = "bar";
			return text.data();
		});
	EXPECT_STREQ("foo/bar/test.txt", result.c_str());
}
}
