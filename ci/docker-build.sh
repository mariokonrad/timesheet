#!/bin/bash -e

export SCRIPT_BASE=$(dirname `readlink -f $0`)
export BASE=${SCRIPT_BASE}/..
export BUILD=${BASE}/build

supported_build_types=("Debug" "Release")
name=marnav
account=mariokonrad/

export NUM_PROC=${NUM_PROC:-$(nproc --ignore=2)}


function docker_run()
{
	dockerid=$1
	cmd="$2"

	docker run \
		--rm \
		--read-only \
		--volume $(pwd):$(pwd) \
		--workdir $(pwd) \
		--env HOME=$(pwd) \
		--user $(id -u):$(id -g) \
		${dockerid} \
		bash -c "${cmd}"
}

function build()
{
	compiler=$1
	build_type=$2
	dockerid=${account}${name}:${compiler}
	builddir=${BUILD}

	docker_run ${dockerid} "cmake -B ${builddir} -DCMAKE_BUILD_TYPE=${build_type} ${BASE}"
	docker_run ${dockerid} "cmake --build ${builddir} -j ${NUM_PROC}"
	docker_run ${dockerid} "${builddir}/testrunner"
}

function usage()
{
	echo "usage: $(basename $0) compiler build-type"
	echo ""
	echo "  supported build types:"
	for v in ${supported_build_types[@]} ; do
		echo "   - $v"
	done
	echo ""
}

function check_supported_build_types()
{
	build_type=$1
	for v in ${supported_build_types[@]} ; do
		if [ "${v}" == "${build_type}" ] ; then
			return
		fi
	done
	echo "error: specified build type not supported: ${build_type}"
	exit -1
}

if [ $# -ne 2 ] ; then
	usage
	exit 1
fi

check_supported_build_types $2
build $1 $2

