## TIMESHEET

(c) 2021 Mario Konrad

Timesheet explorer.

## Help information

Execute:
```
timesheet -h
```

## Example Configuration


```
{
	"minutes_per_workday" : {
		".." : 480
	},
	"input_filepath" : [
		"/some/path/office/company/company.txt",
		"{HOME}/path/office/company/company2.txt"
	]
}
```

## Example timesheet

`demo.txt`:
```
2021-08-18 08:00-12:00               bar
2021-08-17 08:00-09:00; 10:00-11:00  foo
```

Execution:
```
timesheet -f demo.txt -d -t daily ..
```

yields
```
2021-08-17 02:00
2021-08-18 04:00
```

Execution:
```
timesheet -f demo.txt -d ..
```
yields:
```
2021-08-17 08:00-09:00 : foo
2021-08-17 10:00-11:00 : foo
2021-08-18 08:00-12:00 : bar
```


## Build

```
cmake -B build
cmake --build build
```

Requirements:
- C++17 capable compiler
- cmake 3.18 or newer

