#ifndef TIMESHEET_PRINT_COMPACT_DAILY_INDUSTRIAL_HPP
#define TIMESHEET_PRINT_COMPACT_DAILY_INDUSTRIAL_HPP

#include "database.hpp"
#include "range.hpp"
#include <iosfwd>

namespace timesheet
{
class config;
class range;

void print_compact_daily_industrial(
	std::ostream & os, const config & cfg, const database & db, range r);
}

#endif
