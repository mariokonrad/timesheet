#ifndef TIMESHEET_PRINT_TRANSFER_HPP
#define TIMESHEET_PRINT_TRANSFER_HPP

#include "database.hpp"
#include "range.hpp"
#include <iosfwd>

namespace timesheet
{
class config;
class range;

void print_transfer(std::ostream & os, const config & cfg, const database & db, range r);
}

#endif
