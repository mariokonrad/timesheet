#ifndef TIMESHEET_DATABASE_HPP
#define TIMESHEET_DATABASE_HPP

#include "date.hpp"

#include <chrono>
#include <map>
#include <string>
#include <variant>
#include <vector>

namespace timesheet
{
struct time_range {
	std::chrono::minutes start; // duration since midnight
	std::chrono::minutes end; // duration since midnight

	std::chrono::minutes duration() const;
};

using time_description = std::variant<std::chrono::minutes, time_range>;

enum command { none, transfer };

command to_command(const std::string & s);

struct db_date_entry {
	time_description time;
	std::string text;
	command cmd = command::none;

	std::chrono::minutes duration() const;
};

using database = std::map<date, std::vector<db_date_entry>>;

std::string to_string(const time_range & tr);
std::string to_string(const db_date_entry & entry);

void read_data_txt(database & db, std::istream & is);
database read_files_into_db(const std::vector<std::string> & filepaths);
}

#endif
