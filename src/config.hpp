#ifndef TIMESHEET_CONFIG_HPP
#define TIMESHEET_CONFIG_HPP

#include "date.hpp"
#include "range.hpp"
#include <chrono>
#include <functional>
#include <string>
#include <vector>
#include <cstdlib>

namespace timesheet
{
class config
{
public:
	config() = default;

	config(const config &) = default;
	config & operator=(const config &) = default;

	config(config &&) = default;
	config & operator=(config &&) = default;

	void add_minutes_per_day(range r, std::chrono::minutes value);
	std::chrono::minutes minutes_per_day(date d) const;

	void insert_input_filepath(std::string path);
	void override_input_filepath(const std::vector<std::string> & paths);
	const std::vector<std::string> & input_filepaths() const;

private:
	struct minutes_per_day_in_range {
		range r;
		std::chrono::minutes m;

		minutes_per_day_in_range(const range & r, const std::chrono::minutes & m)
			: r(r)
			, m(m)
		{
		}
	};

	std::vector<minutes_per_day_in_range> minutes_per_day_;
	std::vector<std::string> input_filepaths_;
};

std::string replace_environment_variables(
	const std::string & s, std::function<char *(const char *)> get_env = ::getenv);

std::string default_config_filepath();
std::string default_input_filepath();

config read_config_string(const std::string & s);
config read_config_file(const std::string & path);
}

#endif
