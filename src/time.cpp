#include "time.hpp"

#include <fmt/format.h>

namespace timesheet
{
std::chrono::minutes to_minutes(std::string s)
{
	if (s.empty())
		throw std::invalid_argument {"empty string in to_minutes"};

	const std::string::size_type start_index = ((s.front() == '+') || (s.front() == '-')) ? 1 : 0;
	const int sign = (s.front() == '-') ? -1 : 1;

	const auto n = s.find_first_of(":");
	const auto hours = std::stoul(s.substr(start_index, n));
	const auto minutes = std::stoul(s.substr(n + 1));
	return sign * (std::chrono::minutes(minutes) + std::chrono::hours(hours));
}

std::string render_duration(std::chrono::minutes d)
{
	d = abs(d);
	const auto hours = std::chrono::duration_cast<std::chrono::hours>(d).count();
	const auto minutes = d.count() % 60;

	return fmt::format("{0:02}:{1:02}", hours, minutes);
}

std::string render_duration_signed(std::chrono::minutes d)
{
	auto t = abs(d);
	const char sign = (t == d) ? '+' : '-';
	d = t;

	const auto hours = std::chrono::duration_cast<std::chrono::hours>(d).count();
	const auto minutes = d.count() % 60;

	return fmt::format("{0}{1:02}:{2:02}", sign, hours, minutes);
}

std::string render_duration_industrial(std::chrono::minutes d)
{
	char sign = ' ';
	if (d < std::chrono::minutes {0}) {
		d = -d;
		sign = '-';
	}
	double hours
		= std::chrono::duration_cast<std::chrono::hours>(d).count() + (d.count() % 60) / 60.0;
	return fmt::format("{0}{1: >5.2f}", sign, hours);
}
}

