#include "range.hpp"
#include <ostream>
#include <regex>

namespace timesheet
{
range::range(const date & f, const date & t)
	: from_(f)
	, to_(t)
{
	if (from_ > to_)
		throw std::invalid_argument{"'from' greater 'to'"};
}

range::range(const std::pair<date, date> & ft)
	: range(ft.first, ft.second)
{
}

date range::from() const
{
	return from_;
}

date range::to() const
{
	return to_;
}

bool range::contains(const date & d) const
{
	return (d >= from()) && (d <= to());
}

static range parse_special(const std::string & special, int offset)
{
	if (special == "today")
		return {date::today(offset), date::today(offset)};
	if (special == "week")
		return {date::this_week(offset)};
	if (special == "month")
		return {date::this_month(offset)};
	if (special == "year")
		return {date::this_year(offset)};
	throw std::runtime_error {"invalid special value"};
}

range range::parse(const std::string & s)
{
	// clang-format off
	static const std::basic_regex<char> pattern(
		// range
		"(([0-9]{4})(-(01|02|03|04|05|06|07|08|09|10|11|12)(-([0-9]{2}))?)?)?"
		"\\.\\."
		"(([0-9]{4})(-(01|02|03|04|05|06|07|08|09|10|11|12)(-([0-9]{2}))?)?)?"
		// single_date
		"|([0-9]{4})(-(01|02|03|04|05|06|07|08|09|10|11|12)(-([0-9]{2}))?)?"
		// special values
		"|(today|week|month|year)(-[1-9][0-9]*)?"
		);
	// clang-format on

	std::cmatch result;
	const auto rc = regex_match(s.c_str(), result, pattern);
	if (!rc)
		throw std::runtime_error{"parse error (regex)"};

	if (result.size() != 20u)
		throw std::runtime_error{"parse error (number of fields)"};

	// special values
	if (result[18].length()) {
		int offset = 0;
		if (result[19].length())
			offset = std::stoi(result[19]);

		return parse_special(result[18].str(), offset);
	}

	// single date
	if (result[13].length())
		return {date::parse_floor(result[13].str(), result[15].str(), result[17].str()),
			date::parse_ceil(result[13].str(), result[15].str(), result[17].str())};

	// range
	return {date::parse_floor(result[2].str(), result[4].str(), result[6].str()),
		date::parse_ceil(result[8].str(), result[10].str(), result[12].str())};
}

::date::days range::duration() const
{
	return difference(to(), from());
}

std::ostream & operator<<(std::ostream & os, const range & r)
{
	return os << r.from() << ".." << r.to();
}
}
