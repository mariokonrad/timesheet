#ifndef TIMESHEET_PRINT_DAILY_HPP
#define TIMESHEET_PRINT_DAILY_HPP

#include "database.hpp"
#include "range.hpp"
#include <iosfwd>

namespace timesheet
{
class config;
class range;

void print_daily(std::ostream & os, const config & cfg, const database & db, range r);
}

#endif
