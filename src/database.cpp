#include "database.hpp"
#include "time.hpp"

#include <fmt/format.h>

#include <filesystem>
#include <fstream>
#include <sstream>

namespace timesheet
{
std::chrono::minutes time_range::duration() const
{
	return std::chrono::duration_cast<std::chrono::minutes>(end - start);
}

std::chrono::minutes db_date_entry::duration() const
{
	switch (time.index()) {
		case 0:
			return std::get<0>(time);
		case 1:
			return std::get<1>(time).duration();
	}
	throw std::runtime_error {"invalid index in db_date_entry::time"};
}

std::string to_string(const time_range & tr)
{
	return fmt::format("{0}-{1}", render_duration(tr.start), render_duration(tr.end));
}

std::string to_string(const db_date_entry & entry)
{
	switch (entry.time.index()) {
		case 0:
			return render_duration(std::get<0>(entry.time));
		case 1:
			return to_string(std::get<1>(entry.time));
	}
	throw std::runtime_error {"invalid index in db_date_entry::time"};
}

static time_description parse_duration(std::string s)
{
	return time_description {to_minutes(s)};
}

static time_description parse_time_range(std::string s, std::string e)
{
	return time_description {time_range {to_minutes(s), to_minutes(e)}};
}

static time_description create_time_description(std::string s)
{
	if (s.back() == ';')
		s = s.substr(0, s.size() - 1);
	const auto n = s.find_first_of("-");
	if (n == std::string::npos) {
		return parse_duration(s);
	}
	return parse_time_range(s.substr(0, n), s.substr(n + 1));
}

command to_command(const std::string & s)
{
	if (s == "transfer")
		return command::transfer;
	return command::none;
}

void read_data_txt(database & db, std::istream & is)
{
	enum class state { start, command_time, time, before_text, text };

	for (std::string line; std::getline(is, line);) {
		if (line.empty())
			continue;

		// trim leading whitespaces
		line = line.substr(line.find_first_not_of(" \t\f\n\r"));
		if (line.empty())
			continue;

		// ignore commented lines
		if (line.front() == '#')
			continue;

		std::istringstream is(line);

		date d;
		is >> d;

		command cmd = command::none;
		std::string text;
		std::vector<time_description> td;

		state st = state::start;
		std::string field;

		const auto transition_to_time_add_desc = [&]() {
			st = state::time;
			try {
				td.push_back(create_time_description(field));
			} catch (...) {
				// exception intentionally ignored,
				// invalid fields not handled
			}
		};

		const auto transition_to_command_time_save_cmd = [&]() {
			st = state::command_time;
			cmd = to_command(field);
			if (cmd == command::none)
				throw std::runtime_error {fmt::format("invalid command: {}", field).c_str()};
		};

		const auto transition_to_text = [&]() {
			st = state::text;
			text = field;
		};

		const auto transition_to_before_text_add_cmd_time = [&]() {
			st = state::before_text;
			td.emplace_back(to_minutes(field));
		};

		const auto transition_to_text_appending = [&]() {
			st = state::text;
			text += ' ' + field;
		};

		while (is >> field) {
			switch (st) {
				case state::start:
					if (std::isdigit(field.front())) {
						transition_to_time_add_desc();
					} else {
						transition_to_command_time_save_cmd();
					}
					break;

				case state::command_time:
					if (field.front() == '+' || field.front() == '-') {
						transition_to_before_text_add_cmd_time();
					} else {
						transition_to_text();
					}
					break;

				case state::time:
					if (std::isdigit(field.front())) {
						transition_to_time_add_desc();
					} else {
						transition_to_text();
					}
					break;

				case state::before_text:
					transition_to_text();
					break;

				case state::text:
					transition_to_text_appending();
					break;
			}
		}
		if (cmd != command::none && td.empty())
			throw std::runtime_error{"error: no time duration for command"};
		for (const auto & t : td)
			db[d].push_back(db_date_entry {t, text, cmd});
	}
}

database read_files_into_db(const std::vector<std::string> & filepaths)
{
	database db;
	for (const auto & path : filepaths) {
		const std::filesystem::path filepath = path;
		if (!std::filesystem::exists(filepath))
			throw std::runtime_error {fmt::format("error: file not found: {0}\n", path)};

		const auto ext = filepath.extension().string();
		if (ext == ".txt") {
			std::ifstream ifs(filepath.string());
			read_data_txt(db, ifs);
		} else {
			throw std::runtime_error {
				fmt::format("error: file format '{0}' not supported.\n", ext)};
		}
	}
	return db;
}
}

