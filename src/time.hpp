#ifndef TIMESHEET_TIME_HPP
#define TIMESHEET_TIME_HPP

#include <chrono>
#include <string>

namespace timesheet
{
std::chrono::minutes to_minutes(std::string s);

std::string render_duration(std::chrono::minutes d);
std::string render_duration_signed(std::chrono::minutes d);
std::string render_duration_industrial(std::chrono::minutes d);
}

#endif
