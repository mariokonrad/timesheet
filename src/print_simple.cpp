#include "print_simple.hpp"
#include <fmt/format.h>
#include <ostream>

namespace timesheet
{
/// Prints a simple list of work time bookings, with date, work time on each item.
/// Commands are completely ignored.
void print_simple(std::ostream & os, const config &, const database & db, range r)
{
	for (const auto & [d, v] : db) {
		if (!r.contains(d))
			continue;

		for (const auto & w : v) {
			if (w.cmd != command::none)
				continue;

			os << fmt::format("{0} {1:<11} : {2}\n", d.str(), to_string(w), w.text);
		}
	}
}
}
