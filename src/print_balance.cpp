#include "print_balance.hpp"
#include "calc_balance.hpp"
#include "config.hpp"
#include "time.hpp"
#include <fmt/format.h>
#include <chrono>
#include <ostream>

namespace timesheet
{
/// Prints the total balance for the speicied range.
/// This printer recognizes commands and acts accordingly.
void print_balance(std::ostream & os, const config & cfg, const database & db, range r)
{
	const auto b = calc_balance(cfg, db, r);
	const auto t = b.value - b.transfer;
	const auto avg = b.total / b.days;

	os << fmt::format("{} days\n", b.days);

	os << fmt::format("  balance: {} ({} minutes, {:.2f} hours)\n",
		render_duration_signed(b.value), b.value.count(), b.value.count() / 60.0);

	os << fmt::format("  raw    : {} ({} minutes, {:.2f} hours) (transfer: {:.2f} hours)\n",
		render_duration_signed(t), t.count(), t.count() / 60.0, b.transfer.count() / 60.0);

	os << fmt::format("  average:  {} ({} minutes, {:.2f} hours)\n",
		render_duration(avg), avg.count(), avg.count() / 60.0);
}
}
