#ifndef TIMESHEET_VERISION_HPP
#define TIMESHEET_VERISION_HPP

#include <string>

namespace timesheet
{
std::string project_name();
std::string version();
std::string version_major();
std::string version_minor();
std::string version_patch();
std::string version_tweak();
}

#endif
