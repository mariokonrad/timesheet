#include "config.hpp"
#include "database.hpp"
#include "range.hpp"
#include "version.hpp"
#include "print_balance.hpp"
#include "print_brief.hpp"
#include "print_simple.hpp"
#include "print_daily.hpp"
#include "print_daily_industrial.hpp"
#include "print_compact_daily_industrial.hpp"
#include "print_summary_daily_industrial.hpp"
#include "print_transfer.hpp"

#include <fmt/format.h>

#include <cxxopts.hpp>

#include <functional>
#include <iostream>
#include <ostream>
#include <string>
#include <vector>

#include <cctype>

namespace timesheet
{
struct printer_map_entry {
	std::vector<std::string> alias;
	std::function<void(std::ostream &, const config &, const database &, range)> print;
};

static const std::vector<printer_map_entry> printers = {
	{{"s", "simple"}, print_simple},
	{{"b", "brief"}, print_brief},
	{{"d", "daily"}, print_daily},
	{{"id", "industryday"}, print_daily_industrial},
	{{"cid", "compactindustryday"}, print_compact_daily_industrial},
	{{"sid", "summaryindustryday"}, print_summary_daily_industrial},
	{{"tr", "transfer"}, print_transfer},
};

static std::string default_dump_type()
{
	return {"simple"};
}

static std::string list_dump_types()
{
	std::string result;
	for (const auto & i : printers)
		result += fmt::format("- {}\n", fmt::join(i.alias, ", "));
	return result;
}

static std::vector<printer_map_entry>::const_iterator determine_printer(const std::string & arg)
{
	return std::find_if(begin(printers), end(printers), [&](const auto & entry) {
		return std::find(begin(entry.alias), end(entry.alias), arg) != end(entry.alias);
	});
}
}

static void handle_unhandled_exception(std::exception_ptr e)
{
	try {
		std::rethrow_exception(e);
	} catch (std::exception & e) {
		fmt::print("error: {}\n", e.what());
	}
}

int main(int argc, char ** argv)
{
	using namespace timesheet;

	std::set_terminate([](){
		handle_unhandled_exception(std::current_exception());
		std::exit(-1);
	});

	struct {
		std::vector<std::string> input_filepaths;
		std::string config_filepath = default_config_filepath();
		std::string range = "..";
		std::string dump_printer = default_dump_type();
	} opt;

	// clang-format off
	auto options = cxxopts::Options{argv[0], project_name() + " - handle your time"};
	options.add_options("General")
		("h,help", "prints help information")
		("v,version", "prints version information")
		("c,config", "configuration file",
			cxxopts::value<std::string>(opt.config_filepath)
			->default_value(opt.config_filepath))
		("f,file", "input file, can be specified multiple times, input files stack",
			cxxopts::value<std::vector<std::string>>(opt.input_filepaths))
		;
	options.add_options("Query")
		("d,dump", "prints data")
		("t,dump-type", "specify printer, possible values:\n" + list_dump_types(),
			cxxopts::value<std::string>(opt.dump_printer)
			->default_value(opt.dump_printer))
		("b,balance", "prints balance")
		("range", "range TODO: positional argument",
			cxxopts::value<std::string>(opt.range))
		;
	// clang-format on

	options.parse_positional("range");
	options.positional_help("range");

	const auto args = options.parse(argc, argv);

	if (args.unmatched().size() != 0) {
		fmt::print("invalid arguments, use --help to show valid options\n");
		return -1;
	}

	if (args.count("help")) {
		fmt::print("{}\n", options.help());
		fmt::print(" Range:\n"
				   "  ..       : everything\n"
				   "  from..to : range of dates\n"
				   "  from..   : range of dates, starting at from until most recent\n"
				   "  ..to     : range of dates, starting at the beginning until end date\n"
				   "  speific  : special identifier for a range\n");
		fmt::print("\n");
		fmt::print(" With:\n"
				   "  from, to : date in the forms YYYY-MM-DD, YYYY-MM or YYYY\n"
				   "  specific : named range, one of: year, month, week, today\n"
				   "             can be altered with '-N', example last week: week-1\n");
		fmt::print("\n");
		return 0;
	}

	if (args.count("version")) {
		fmt::print("{}\n", version());
		return 0;
	}

	if (args.count("range") != 1u) {
		fmt::print("error: one range required\n");
		return -1;
	}

	auto cfg = read_config_file(opt.config_filepath);

	if (args.count("file") > 0u)
		cfg.override_input_filepath(opt.input_filepaths);
	else if (cfg.input_filepaths().empty())
		cfg.insert_input_filepath(default_input_filepath());

	auto db = read_files_into_db(cfg.input_filepaths());

	if (args.count("balance"))
		print_balance(std::cout, cfg, db, range::parse(opt.range));

	if (args.count("dump")) {
		auto p = determine_printer(opt.dump_printer);
		if (p == end(printers))
			throw std::runtime_error {"invalid dump printer type: " + opt.dump_printer};
		p->print(std::cout, cfg, db, range::parse(opt.range));
	}

	return 0;
}
