#ifndef TIMESHEET_DATE_HPP
#define TIMESHEET_DATE_HPP

#include <date/date.h>
#include <iosfwd>
#include <utility>
#include <string>

namespace timesheet
{
class date
{
public:
	using value_type = int32_t;

	static constexpr value_type min_year = 0;
	static constexpr value_type min_month = 1;
	static constexpr value_type min_day = 1;
	static constexpr value_type max_year = 9999;
	static constexpr value_type max_month = 12;

	static constexpr value_type max_mday(value_type year, value_type month) noexcept
	{
		constexpr const value_type t[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		return is_leap_year(year) && (month == 2) ? t[month] + 1 : t[month];
	}

	static constexpr bool is_leap_year(value_type year) noexcept
	{
		return ((year % 4) == 0) && (((year % 100) != 0) || ((year % 400) == 0));
	}

	date() = default;

	date(value_type year, value_type month, value_type day);
	explicit date(::date::year_month_day ymd);

	std::string str() const;

	static date min() { return {min_year, min_month, min_day}; }
	static date max() { return {max_year, max_month, 31}; }

	static date parse(const std::string & s);
	static date parse_floor(
		const std::string & ys, const std::string & ms, const std::string & ds);
	static date parse_ceil(
		const std::string & ys, const std::string & ms, const std::string & ds);

	/// Returns the date of today, local time.
	static date today();

	/// Returns the date of today (plus offset in number of days), local time.
	///
	/// Offset is additive, this means only negative offsets make sense.
	static date today(int offset);

	/// Returns the start and end of the current week.
	/// This is not configurable, start of the week is monday, end of the week is sunday.
	static std::pair<date, date> this_week();
	static std::pair<date, date> this_week(int offset);

	/// Returns the start and end of the current month.
	static std::pair<date, date> this_month();
	static std::pair<date, date> this_month(int offset);

	/// Returns the start and end of the current month.
	static std::pair<date, date> this_year();
	static std::pair<date, date> this_year(int offset);

	friend bool operator==(const date & a, const date & b) noexcept;
	friend bool operator!=(const date & a, const date & b) noexcept;
	friend bool operator<(const date & a, const date & b) noexcept;
	friend bool operator<=(const date & a, const date & b) noexcept;
	friend bool operator>(const date & a, const date & b) noexcept;
	friend bool operator>=(const date & a, const date & b) noexcept;


	value_type year() const noexcept { return year_; }
	value_type month() const noexcept { return month_; }
	value_type day() const noexcept { return day_; }

private:
	value_type year_ = min_year;
	value_type month_ = min_month;
	value_type day_ = min_day;
};

/// Converts a date to year_month_day. To avoid implicit conversions, this
/// function is to make conversions explicit and therefore visible.
::date::year_month_day to_ymd(const date &);

/// Returns the number of days between the two spcified dates.
::date::days difference(const date & a, const date & b);

std::istream & operator>>(std::istream & is, date & d);
std::ostream & operator<<(std::ostream & os, const date & d);
}

#endif
