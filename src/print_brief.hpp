#ifndef TIMESHEET_PRINT_BRIEF_HPP
#define TIMESHEET_PRINT_BRIEF_HPP

#include "database.hpp"
#include "range.hpp"
#include <iosfwd>

namespace timesheet
{
class config;
class range;

void print_brief(std::ostream & os, const config & cfg, const database & db, range r);
}

#endif
