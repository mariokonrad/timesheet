#include "print_transfer.hpp"
#include "time.hpp"
#include <fmt/format.h>
#include <chrono>
#include <ostream>

namespace timesheet
{
/// Prints an overview of transfers for the speicied range.
void print_transfer(std::ostream & os, const config &, const database & db, range r)
{
	auto total = std::chrono::minutes{0};

	for (const auto & [d, v] : db) {
		if (!r.contains(d))
			continue;

		auto daily_total = std::chrono::minutes{0};

		for (const auto & w : v) {
			if (w.cmd != command::transfer)
				continue;

			daily_total += w.duration();
			total += w.duration();
		}

		if (daily_total.count() == 0)
			continue;

		os << fmt::format("{}  {}  ({})\n", d.str(), render_duration_signed(daily_total),
			render_duration_industrial(daily_total));
	}

	os << fmt::format("\nTOTAL:  {}  ({})\n", render_duration_signed(total),
		render_duration_industrial(total));
}
}

