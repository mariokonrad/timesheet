#include "print_compact_daily_industrial.hpp"
#include "time.hpp"
#include <fmt/format.h>
#include <chrono>
#include <numeric>
#include <map>
#include <ostream>

namespace timesheet
{
/// Prints a list of booked work for each text each day in industrial notation.
/// Commands are completely ignored.
void print_compact_daily_industrial(
	std::ostream & os, const config &, const database & db, range r)
{
	for (const auto & [d, v] : db) {
		if (!r.contains(d))
			continue;

		std::map<std::string, std::chrono::minutes> m;
		for (const auto & w : v) {
			if (w.cmd != command::none)
				continue;
			m[w.text] += w.duration();
		}

		for (const auto & [c, t] : m)
			os << fmt::format("{0} {1:>5} {2}\n", d.str(), render_duration_industrial(t), c);
	}
}
}
