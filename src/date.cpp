#include "date.hpp"

#include <date/date.h>

#include <fmt/format.h>

#include <chrono>
#include <istream>
#include <ostream>
#include <regex>
#include <tuple>

#include <ctime>

namespace
{
static timesheet::date::value_type value(
	const std::string & s, timesheet::date::value_type default_value)
{
	return s.empty() ? default_value : std::stoi(s);
}

static constexpr std::pair<date::year_month_day, date::year_month_day> determine_week(
	date::year_month_day ymd, int offset)
{
	const auto start = (date::sys_days {ymd} + date::days {offset * 7})
		- (date::weekday {ymd} - date::Monday);
	return {date::year_month_day {start}, date::year_month_day {start + date::days {6}}};
}

template <class Clock>
static constexpr std::pair<date::year_month_day, date::year_month_day> determine_week(
	std::chrono::time_point<Clock> tp, int offset)
{
	return determine_week(std::chrono::time_point_cast<date::days>(tp), offset);
}

static constexpr std::pair<date::year_month_day, date::year_month_day> determine_month(
	date::year_month_day ymd, int offset)
{
	const date::year_month_day start
		= {ymd.year(), ymd.month() + date::months {offset}, date::day {1}};
	return {start,
		date::sys_days {date::year_month_day {start} + date::months {1}} - date::days {1}};
}

template <class Clock>
static constexpr std::pair<date::year_month_day, date::year_month_day> determine_month(
	std::chrono::time_point<Clock> tp, int offset)
{
	return determine_month(std::chrono::time_point_cast<date::days>(tp), offset);
}

static constexpr std::pair<date::year_month_day, date::year_month_day> determine_year(
	date::year_month_day ymd, int offset)
{
	const date::year_month_day start
		= {ymd.year() + date::years {offset}, date::month {1}, date::day {1}};
	return {start,
		date::sys_days {date::year_month_day {start} + date::years {1}} - date::days {1}};
}

template <class Clock>
static constexpr std::pair<date::year_month_day, date::year_month_day> determine_year(
	std::chrono::time_point<Clock> tp, int offset)
{
	return determine_year(std::chrono::time_point_cast<date::days>(tp), offset);
}
}

namespace timesheet
{
date::date(value_type year, value_type month, value_type day)
	: year_(year)
	, month_(month)
	, day_(day)
{
	if (year_ < min_year || year_ > max_year)
		throw std::invalid_argument{"invalid year"};
	if (month_ < min_month || month_ > max_month)
		throw std::invalid_argument{"invalid month"};
	if (day_ < min_day || day > max_mday(year_, month_))
		throw std::invalid_argument{"invalid day"};
}

date::date(::date::year_month_day ymd)
	: year_(static_cast<int>(ymd.year()))
	, month_(static_cast<int>(static_cast<unsigned>(ymd.month())))
	, day_(static_cast<int>(static_cast<unsigned>(ymd.day())))
{
}

std::string date::str() const
{
	return fmt::format("{0:04d}-{1:02d}-{2:02d}", year_, month_, day_);
}

bool operator==(const date & a, const date & b) noexcept
{
	return std::tie(a.year_, a.month_, a.day_) == std::tie(b.year_, b.month_, b.day_);
}

bool operator!=(const date & a, const date & b) noexcept
{
	return !(a == b);
}

bool operator<(const date & a, const date & b) noexcept
{
	return std::tie(a.year_, a.month_, a.day_) < std::tie(b.year_, b.month_, b.day_);
}

bool operator<=(const date & a, const date & b) noexcept
{
	return (a < b) || (a == b);
}

bool operator>(const date & a, const date & b) noexcept
{
	return !(a <= b);
}

bool operator>=(const date & a, const date & b) noexcept
{
	return (a > b) || (a == b);
}

::date::days difference(const date & a, const date & b)
{
	const auto ad = ::date::sys_days{to_ymd(a)};
	const auto bd = ::date::sys_days{to_ymd(b)};
	return (b > a) ? (bd - ad) : (ad - bd);
}

::date::year_month_day to_ymd(const date & d)
{
	return {
		::date::year{d.year()},
		::date::month{static_cast<unsigned int>(d.month())},
		::date::day{static_cast<unsigned int>(d.day())}};
}

std::istream & operator>>(std::istream & is, date & d)
{
	std::string s;
	is >> s;
	d = date::parse(s);
	return is;
}

std::ostream & operator<<(std::ostream & os, const date & d)
{
	return os << d.str();
}

date date::parse(const std::string & s)
{
	// clang-format off
	static const std::basic_regex<char> pattern(
		"([0-9]{4})-(01|02|03|04|05|06|07|08|09|10|11|12)-([0-9]{2})");
	// clang-format on

	std::cmatch result;
	const auto rc = regex_match(s.c_str(), result, pattern);
	if (!rc)
		throw std::runtime_error{"parse error"};
	if (result.size() != 4u)
		throw std::runtime_error{"parse error"};

	return parse_floor(result[1].str(), result[2].str(), result[3].str());
}

date date::parse_floor(const std::string & ys, const std::string & ms, const std::string & ds)
{
	return {value(ys, min_year), value(ms, min_month), value(ds, min_day)};
}

date date::parse_ceil(
		const std::string & ys, const std::string & ms, const std::string & ds)
{
	const auto y = value(ys, max_year);
	const auto m = value(ms, max_month);
	const auto d = value(ds, max_mday(y, m));
	return {y, m, d};
}

date date::today()
{
	return today(0);
}

date date::today(int offset)
{
	const auto t = std::chrono::system_clock::to_time_t(
		std::chrono::system_clock::now() + std::chrono::hours{24} * offset);
	struct tm tmp;
#if defined(__GNUC__)
	localtime_r(&t, &tmp);
#else
	localtime_s(&tmp, &t);
#endif
	return {tmp.tm_year + 1900, tmp.tm_mon + 1, tmp.tm_mday};
}

std::pair<date, date> date::this_week()
{
	return this_week(0);
}

std::pair<date, date> date::this_week(int offset)
{
	const auto w = determine_week(std::chrono::system_clock::now(), offset);
	return {date(w.first), date(w.second)};
}

std::pair<date, date> date::this_month()
{
	return this_month(0);
}

std::pair<date, date> date::this_month(int offset)
{
	const auto m = determine_month(std::chrono::system_clock::now(), offset);
	return {date(m.first), date(m.second)};
}

std::pair<date, date> date::this_year()
{
	return this_year(0);
}

std::pair<date, date> date::this_year(int offset)
{
	const auto m = determine_year(std::chrono::system_clock::now(), offset);
	return {date(m.first), date(m.second)};
}
}
