#include "config.hpp"

#include <nlohmann/json.hpp>

#include <fmt/format.h>

#include <filesystem>
#include <fstream>
#include <regex>
#include <sstream>
#include <stdexcept>

namespace timesheet
{
namespace
{
static void read_minutes_per_workday(config & cfg, const nlohmann::json & j)
{
	static const char * id = "minutes_per_workday";

	if (j.count(id) == 0)
		return;

	const auto element = j[id];

	if (!element.is_object())
		throw std::runtime_error{"expected JSON object for minutes_per_day"};

	for (const auto & [k, v] : element.items()) {
		if (v.size() != 1u)
			throw std::runtime_error{"invalid format for range specifier in minutes_per_workday"};

		cfg.add_minutes_per_day(range::parse(k), std::chrono::minutes{v.get<int64_t>()});
	}
}

static void read_input_filepaths(config & cfg, const nlohmann::json & j)
{
	static const char * id = "input_filepath";

	if (j.count(id) == 0)
		return;

	const auto fp = j.at(id);

	if (fp.is_string()) {
		cfg.insert_input_filepath(fp.get<std::string>());
		return;
	}

	if (fp.is_array()) {
		for (const auto & p : fp)
			cfg.insert_input_filepath(p.get<std::string>());
		return;
	}

	throw std::runtime_error {fmt::format("invalid format config file: {0}", id)};
}

static config read_config(std::istream & is)
{
	nlohmann::json j;
	is >> j;

	config cfg;
	read_minutes_per_workday(cfg, j);
	read_input_filepaths(cfg, j);
	return cfg;
}
}

std::string default_config_filepath()
{
	auto h = getenv("HOME");
	if (!h)
		throw std::runtime_error{"env var HOME not available"};
	return h + std::string("/.timesheet");
}

std::string default_input_filepath()
{
	return "timesheet.txt";
}

config read_config_string(const std::string & s)
{
	std::istringstream is(s);
	return read_config(is);
}

config read_config_file(const std::string & path)
{
	std::filesystem::path p(path);

	// default config is optional
	if (path == default_config_filepath()) {
		if (!std::filesystem::exists(p)) {
			return {};
		}
	}

	// a specfied one is not
	if (!std::filesystem::exists(p))
		throw std::runtime_error{fmt::format("file not found: {0}\n", path)};

	std::ifstream ifs{path};
	if (!ifs)
		throw std::runtime_error{fmt::format("cannot open file: {0}\n", path)};
	return read_config(ifs);
}

void config::add_minutes_per_day(range r, std::chrono::minutes value)
{
	if (value <= std::chrono::minutes{0} || value >= std::chrono::hours{24})
		throw std::runtime_error{"invalid value for minutes per day"};
	minutes_per_day_.emplace_back(r, value);
}

std::chrono::minutes config::minutes_per_day(date d) const
{
	static constexpr auto default_duration = std::chrono::hours{8};

	if (minutes_per_day_.empty())
		return default_duration;

	// search for the most preciese range for the specified date
	// to determine the number of minutes minutes per day.
	// last entry wins.
	minutes_per_day_in_range result = { range(), default_duration };
	for (const auto & i : minutes_per_day_)
		if (i.r.contains(d) && (i.r.duration() <= result.r.duration()))
			result = i;

	return result.m;
}

std::string replace_environment_variables(
	const std::string & s, std::function<char *(const char *)> get_env)
{
	struct subst_result {
		std::string text;
		bool change;
	};

	const auto substitute_envvar = [=](const std::string & t) -> subst_result {
		if (t.empty())
			return {t, false};

		if (!(t.front() == '{' && t.back() == '}')) // no variable
			return {t, false};

		const auto var = t.substr(1, t.size() - 2); // cut away '{' and '}'
		const auto env = get_env(var.c_str());
		if (!env)
			throw std::runtime_error {fmt::format("environment variable not found: {}", var)};

		return {env, true};
	};

	static const std::regex pattern("\\{[a-zA-Z0-9_]+\\}");
	const std::sregex_token_iterator end;

	std::string result = s;
	for (bool changes = true; changes;) {
		changes = false;
		std::string t = result;
		result.clear();

		std::sregex_token_iterator begin(t.begin(), t.end(), pattern, {-1, 0});
		std::for_each(
			begin, end, [&changes, &result, &substitute_envvar](const std::string & t) {
				const auto sr = substitute_envvar(t);
				result += sr.text;
				changes |= sr.change;
			});
	};

	return result;

	/*
	// works on GCC-10 but is most probably implementation depdenent regarding
	// dereferencing pb which is at pe

	const auto substitute_envvar = [&](const std::smatch & m) -> std::string {
		const auto var = m.str(0).substr(1, m.str(0).size() - 2);
		const auto env = get_env(var.c_str());
		if (!env)
			throw std::runtime_error {fmt::format("environment variable not found: {}", var)};
		return {env};
	};

	static const std::regex pattern("\\{[a-zA-Z0-9_]+\\}");

	const std::sregex_iterator pe;

	for (std::string result = s;;) {
		std::ostringstream os;
		std::sregex_iterator pb = {result.begin(), result.end(), pattern};
		for (; pb != pe; ++pb)
			os << pb->prefix() << substitute_envvar(*pb);

		// no substituions performed anymore, nothing more to be done
		if (!pb->ready())
			return result;

		os << result.substr(result.size() - pb->position());
		result = os.str();
	}
	*/
}

void config::insert_input_filepath(std::string path)
{
	if (path.empty())
		throw std::invalid_argument {"invalid argument, input_filepath"};

	path = replace_environment_variables(path);

	if (std::find(begin(input_filepaths_), end(input_filepaths_), path)
		!= end(input_filepaths_))
		return;

	input_filepaths_.push_back(path);
}

void config::override_input_filepath(const std::vector<std::string> & paths)
{
	input_filepaths_ = paths;
}

const std::vector<std::string> & config::input_filepaths() const
{
	return input_filepaths_;
}
}
