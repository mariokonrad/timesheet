#include "print_brief.hpp"
#include "config.hpp"
#include "time.hpp"
#include <fmt/format.h>
#include <chrono>
#include <numeric>
#include <ostream>

namespace timesheet
{
/// Prints brief and compact list of work bookings, sorted by date listing the total work time
/// for each day in normal and industrial notation. It also lists briefly each individual text.
/// Commands are completely ignored.
void print_brief(std::ostream & os, const config & cfg, const database & db, range r)
{
	for (const auto & [d, v] : db) {
		if (!r.contains(d))
			continue;

		const auto mpd = std::accumulate(
			begin(v), end(v), std::chrono::minutes {0}, [](auto value, const auto & w) {
				return (w.cmd == command::none) ? value + w.duration() : value;
			});

		os << fmt::format("{0} {1} ({2:>5} {3:>6}) :", d.str(), render_duration(mpd),
			render_duration_industrial(mpd),
			render_duration_signed(mpd - std::chrono::minutes {cfg.minutes_per_day(d)}));

		for (const auto & w : v) {
			if (w.cmd != command::none)
				continue;

			os << fmt::format(" {0} ({1})", render_duration(w.duration()), w.text);
		}

		os << fmt::format("\n");
	}
}
}
