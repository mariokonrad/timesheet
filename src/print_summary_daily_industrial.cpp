#include "print_summary_daily_industrial.hpp"
#include "calc_balance.hpp"
#include "time.hpp"
#include <fmt/format.h>
#include <map>
#include <numeric>
#include <ostream>

namespace timesheet
{
/// Prints a daily summary of work time, plus a list of time for each text in industrial notation.
/// Commands are completely ignored.
void print_summary_daily_industrial(
	std::ostream & os, const config & cfg, const database & db, range r)
{
	auto n = db.size();

	for (const auto & [d, v] : db) {
		--n;

		if (!r.contains(d))
			continue;

		const auto daily_total = std::accumulate(begin(v), end(v), std::chrono::minutes{0},
			[](auto value, const auto & w) { return (w.cmd == command::none) ? value + w.duration() : value; });

		std::map<std::string, std::chrono::minutes> m;
		for (const auto & w : v) {
			if (w.cmd != command::none)
				continue;
			m[w.text] += w.duration();
		}

		os << fmt::format("{0} : {1}\n", d.str(), render_duration_industrial(daily_total));

		for (const auto & [c, t] : m)
			os << fmt::format("             {0} {1}\n", render_duration_industrial(t), c);

		if (n > 0)
			os << '\n';
	}

	os << "Summary:\n";

	const auto b = calc_balance(cfg, db, r);
	const auto t = b.value - b.transfer;
	const auto avg = b.total / b.days;
	os << fmt::format("  days   : {}\n", b.days);
	os << fmt::format("  balance: {} ({} minutes, {:.2f} hours)\n",
		render_duration_signed(b.value), b.value.count(), b.value.count() / 60.0);
	os << fmt::format("  raw    : {} ({} minutes, {:.2f} hours) (transfer: {:.2f} hours)\n",
		render_duration_signed(t), t.count(), t.count() / 60.0, b.transfer.count() / 60.0);
	os << fmt::format("  average:  {} ({} minutes, {:.2f} hours)\n",
		render_duration(avg), avg.count(), avg.count() / 60.0);
}
}

