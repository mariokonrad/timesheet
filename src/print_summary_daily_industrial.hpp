#ifndef TIMESHEET_PRINT_SUMMARY_DAILY_INDUSTRIAL_HPP
#define TIMESHEET_PRINT_SUMMARY_DAILY_INDUSTRIAL_HPP

#include "database.hpp"
#include "range.hpp"
#include <iosfwd>

namespace timesheet
{
class config;
class range;

void print_summary_daily_industrial(
	std::ostream & os, const config & cfg, const database & db, range r);
}

#endif

