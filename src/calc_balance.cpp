#include "calc_balance.hpp"
#include "config.hpp"
#include "config.hpp"
#include <numeric>

namespace timesheet
{
balance calc_balance(const config & cfg, const database & db, range r)
{
	balance result;

	const auto transfer_duration = [](const db_date_entry & w) {
		return (w.cmd == command::transfer) ? w.duration() : std::chrono::minutes {0};
	};

	const auto absolute_duration = [](const db_date_entry & w) {
		return (w.cmd == command::none) ? w.duration() : std::chrono::minutes {0};
	};

	for (const auto & d : db) {
		if (!r.contains(d.first))
			continue;

		++result.days;

		// time reduced by the duration of the work day for the specific day
		result.value -= cfg.minutes_per_day(d.first);

		result = std::accumulate(begin(d.second), end(d.second), result,
			[&](const balance & b, const auto & w) -> balance {
				return {b.days, b.value + w.duration(), b.transfer + transfer_duration(w),
					b.total + absolute_duration(w)};
			});
	}

	return result;
}
}
