#include "print_daily_industrial.hpp"
#include "time.hpp"
#include <fmt/format.h>
#include <chrono>
#include <numeric>
#include <ostream>

namespace timesheet
{
/// Prints a daily overview of total work time in industrial notation.
/// Commands are completely ignored.
void print_daily_industrial(std::ostream & os, const config &, const database & db, range r)
{
	for (const auto & [d, v] : db) {
		if (!r.contains(d))
			continue;

		const auto total = std::accumulate(
			begin(v), end(v), std::chrono::minutes {0}, [](auto value, const auto & w) {
				return (w.cmd == command::none) ? value + w.duration() : value;
			});

		os << fmt::format("{0} {1}\n", d.str(), render_duration_industrial(total));
	}
}
}
