#ifndef TIMESHEET_RANGE_HPP
#define TIMESHEET_RANGE_HPP

#include "date.hpp"
#include <iosfwd>
#include <date/date.h>
#include <utility>
#include <type_traits>

namespace timesheet
{
class range
{
public:
	static range parse(const std::string &);

	range() = default;
	range(const date & f, const date & t);
	range(const std::pair<date, date> & ft);

	date from() const;
	date to() const;

	/// Compares the specified date against the range.
	///
	/// Returns true if the date is contained (inclusive)
	/// the range `from..to`.
	bool contains(const date & d) const;

	/// Returns the duration of the range in hours.
	::date::days duration() const;

private:
	date from_ = date::min();
	date to_ = date::max();
};

std::ostream & operator<<(std::ostream & os, const range & r);

static_assert(std::is_standard_layout_v<range>);
}

#endif
