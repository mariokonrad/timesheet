#ifndef TIMESHEET_CALC_BALANCE_HPP
#define TIMESHEET_CALC_BALANCE_HPP

#include "database.hpp"
#include "range.hpp"
#include <chrono>

namespace timesheet
{
class config;

struct balance {
	int64_t days = 0;
	std::chrono::minutes value {0};
	std::chrono::minutes transfer {0};
	std::chrono::minutes total {0};
};

balance calc_balance(const config & cfg, const database & db, range r);
}

#endif
